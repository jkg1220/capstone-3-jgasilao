import { useState, useEffect, useContext} from "react"
import {Table, Button, Container,Row, Modal, Form} from 'react-bootstrap';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ShowOrders() {

    const [showAllOrders, setShowAllOrders] = useState();
    const viewAllOrders=()=>{
          fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`,{
        method: "GET",
        headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`  
      }
    })
    .then(res => res.json())
    .then(data => {
        setShowAllOrders(data.map(order =>{
            console.log(data);

    return(
       
        <tr key={order}>
        <td>{order._id}</td>
        <td>{order.userId}</td>
   {/*     <td>{order.product}</td>*/}
       {/* <td>{order.email}</td>*/}
        <td>{order.totalAmount}</td>
        <td>{order.status}</td>
        </tr>
      
    )
        }))
    })

    }
  
    useEffect(()=>{
        viewAllOrders();
    },[])

        /*fetch(`${process.env.REACT_APP_API_URL}/users/user-details/${}`,{
            method: "GET",
            headers: {

            }
        })*/


    return(
        <>
        <Table striped bordered hover className="mt-3 text-center">
            <thead>
            <tr>
              <th>Order ID</th>
              <th>UserId</th>
             {/* <th>Orders</th>*/}
             {/* <th>User email</th>*/}
              <th>Total Amount</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
           {showAllOrders}
          </tbody>
        </Table>
        </>
    
    )
}
