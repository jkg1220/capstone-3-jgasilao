import {useEffect, useState} from "react";
import {Row} from "react-bootstrap";

import ProductCard from "../components/ProductCard";

export default function Product(){

     const [products, setProduct] = useState([]);

    useEffect(() =>{
        fetch(`${process.env.REACT_APP_API_URL}/products/retrieve-product`)
        .then(res => res.json())
        .then(data => {
            setProduct(data.map(product =>{

                return(
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
    },[])
    return(
        <>
            <h1 className = "my-5 text-center">Products</h1>
            <Row>{products}</Row>
            
            
        </>
    )
}
