import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Accordion} from "react-bootstrap";

export default function UserOrders() {

  	const [orders, setOrders] = useState([]);

    const viewAllOrders = async ()=>{
      fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`,{
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data =>{
        setOrders(data.map(userOrdersList =>{
          console.log(userOrdersList.userId);

          fetch(`${process.env.REACT_APP_API_URL}/users/user-details/${userOrdersList.userId}`, {
                 method: "GET",
                 headers:{
                   "Content-Type": "application/json",
                   "Authorization": `Bearer ${localStorage.getItem('token')}`
                 }
               })
               .then(res => res.json())
               .then(data => { 
                console.log(data);
              
              return ( 
                    <>      
                    <Accordion defaultActiveKey="0">
                              <Accordion.Item eventKey="0">
                                 <Accordion.Header>{data.email}</Accordion.Header>
                                    <Accordion.Body>

                                   </Accordion.Body>
                              </Accordion.Item>
                              </Accordion>
                              
                    </>
                      )   
         })                    

        }))
      })
    }
    useEffect(() =>{
      viewAllOrders();
    }, [])

    return (
      <>
        {orders}
      </>

  )}