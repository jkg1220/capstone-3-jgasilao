import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom";

import Swal from "sweetalert2";
import {Button, Form, Col, Row} from "react-bootstrap";


export default function Registration(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();


	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	console.log(email);
	console.log(password1);
	console.log(password2);

		function registerUser(e){
			e.preventDefault();

			//Checking if the email is still available
			fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
				method: "POST",
				headers:{
					"Content-Type":"application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data){
					Swal.fire({
						title: "Duplicate email found",
						icon: "error",
						text: "This email already exist."
					})
				}
				else{
					fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
						method: "POST",
						headers:{
							"Content-Type": "application/json"
						},
						body: JSON.stringify({

							email: email,
							password: password1
						})
					})
					.then(res => res.json())
					.then(data => {
						console.log(data);

						if(data){
							//Clear input fields
							setEmail("");
							setPassword1("");
							setPassword2("");

							Swal.fire({
								title: "Registration successful",
								icon: "success",
								text: "Welcome to J Home Appliances!"
							})

							//redirect the user to the login page after registration.
							navigate("/login");
						}
						else{
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again."
							})
						}
					})
				}
			})

		}

		//State to determine whether submit button is enabled or not.
		const [isActive, setIsActive] = useState(false);

		// To enable the submit button:
			// No empty input fields.
			// password and verify password should be the same.

		useEffect(()=>{
			if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		},[email, password1, password2])

		return(
			(user.id !== null)
			?
				<Navigate to="/login"/>
			:
			<>
				<h1 className="my-5 text-center">Register</h1>		
				<div className = "container">
				<div className = "row">
				<div className = "col-md-12 col-md-offset-4">
				<Form onSubmit = {(e) => registerUser(e)}>

				      <Form.Group className="mb-3" controlId="userEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Verify Password</Form.Label>
				        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
				      </Form.Group>
			      
			      {
			      	isActive
			      	?
			      		<Button variant="primary" type="submit" id="submitBtn">
			      		  Submit
			      		</Button>
			      	:
			      		<Button variant="primary" type="submit" id="submitBtn" disabled>
			      		  Submit
			      		</Button>
			      }
			    </Form>
			    </div>
			    </div>
			    </div>

			</>
		)
	}
