import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";

import { Navigate, Link } from "react-router-dom";
import { Form, Button, Card, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login(){

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function login(e){
		
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to J Home Appliances!"
				});
				window.location.reload(false);

			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}

		})

		//Clear input fields
		setEmail("");
		setPassword("");

	}

	//Retrieve user details
	// we will get the payload from the token.
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() =>{
		
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.id !== null)
		?
			(user.isAdmin)
			?
				<Navigate to="/admin"/>
			:
				<Navigate to="/"/>
		:

		<>
			<div>
		      <Container>
		        <Row className="mt-5 d-flex justify-content-center align-items-center">
		          <Col md={8} lg={6} xs={12}>
		            <Card className="shadow">
		              <Card.Body>
		                <div className="mb-3 mt-md-4">
		                  <h2 className="fw-bold mb-2 text-uppercase text-center">Login</h2>
		                  <div className="mb-3">
		                    <Form onSubmit ={(e) => login(e)}>
		                      <Form.Group className="mb-3" controlId="userEmail">
		                        <Form.Label className="text-center">
		                          Email address
		                        </Form.Label>
		                        <Form.Control type="email" placeholder="Enter email "value={email} onChange={e => setEmail(e.target.value)} />
		                      </Form.Group>

		                      <Form.Group
		                        className="mb-3"
		                        controlId="formBasicPassword"
		                      >
		                        <Form.Label>Password</Form.Label>
		                        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
		                      </Form.Group>
		                      {
		                      	isActive
									?
										<div className="d-grid">
										<Button variant="primary" type="submit" id="submitBtn">
										  Log in
										</Button>
										</div>
									:
										<div className="d-grid">
										<Button variant="primary" type="submit" id="submitBtn" disabled>
										  Log in
										</Button>
										</div>
		                      }

		                    </Form>
		                    <div className="mt-3">
		                      <p className="mb-0  text-center">
		                        Don't have an account yet?{" "}
		                        <Link to="/registration" variant = "body2"> Register </Link> here
		                      </p>
		                    </div>
		                  </div>
		                </div>
		              </Card.Body>
		            </Card>
		          </Col>
		        </Row>
		      </Container>
		    </div>
			
		</>
	)
}