import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";


import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col} from "react-bootstrap";

export default function ProductView() {

	const { productId } = useParams();
	const [name, setName]= useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [subTotal, setSubtotal] = useState(0);

let total = 1;

// For Add Items
	const AddItems = (productId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/product-details/${productId}`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true){
				Swal.fire({
					title:"Item Added Successfully!",
					icon: "success",
				});
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

const createCart= () =>{
	fetch(`${process.env.REACT_APP_API_URL}/orders/order-pending`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.length)
				if(data.length===0){

					fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,{
							method: "POST",
							headers:{
								"Content-Type": "application/json",
								Authorization: `Bearer ${localStorage.getItem("token")}`
							}
						})
						.then(res => res.json())
						.then(data2 => {

									fetch(`${process.env.REACT_APP_API_URL}/orders/addProduct`,{
										method: "POST",
										headers:{
											"Content-Type": "application/json",
											Authorization: `Bearer ${localStorage.getItem("token")}`
										},
										body: JSON.stringify({
											productId: productId,
											totalAmount: subTotal,
											quantity: quantity,
											orderId: data2._id,
											productName: name,
											productPrice: price
										})
									})
									.then(res => res.json())
									.then(data3 => {
										if(data3){

											Swal.fire({
             						title: "Product succesfully Added",
              					icon: "success",
              					text: `${name} is now added`
          						});

										}
										else{
											Swal.fire({
              				title: "Error!",
              				icon: "error",
             					text: `Something went wrong. Please try again later!`
          				});
								}
							})
						})


				}else{

					fetch(`${process.env.REACT_APP_API_URL}/orders/addProduct`,{
										method: "POST",
										headers:{
											"Content-Type": "application/json",
											Authorization: `Bearer ${localStorage.getItem("token")}`
										},
										body: JSON.stringify({
											productId: productId,
											totalAmount: subTotal,
											quantity: quantity,
											orderId: data[0]._id,
											productName: name,
											productPrice: price
										})
									})
									.then(res => res.json())
									.then(data3 => {
											if(data3){
												Swal.fire({
             						title: "Product succesfully Added",
              					icon: "success",
              					text: `${name} is now added`
          						});

											}else{
												Swal.fire({
	              				title: "Error!",
	              				icon: "error",
	             					text: `Something went wrong. Please try again later!`
          				});
								}
						})

				}



		})

}

const fetchProduct = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/product-details/${productId}`)
	.then(res => res.json())
	.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			console.log(data);
	})
}

useEffect(()=>{
  fetchProduct();
  },[])

const plusOne = () =>{
 setQuantity(quantity+1);
total=(quantity*price);
//console.log(price);
//console.log(total + price);

setSubtotal(total+price);
    
}

const minusOne = () =>{
 setQuantity(quantity-1);
total=(quantity*price);
//console.log(price);
//console.log(total - price);

setSubtotal(total - price);

}
	return(

		// Card Details
		<Container className="mt-5">
					<Row>
						<Col lg={{ span: 6, offset: 3 }}>
							<Card className= "text-center">
							   <Card.Header className= "text-center">
							   <h2>{name}</h2>
							   </Card.Header>
							    <Card.Body>
							      <Card.Text>
							       <h4>{description}</h4>
							       </Card.Text>
							       <Card.Text>
							         <h5>Price: Php {subTotal}</h5>
							       </Card.Text>
							       <Card.Text>
							       <h5>{quantity}</h5>
							       </Card.Text>
							       	<Button variant= "primary" size= "sm" onClick ={() => minusOne(productId)}>-</Button>
					        		{' '}
					        		<Button variant= "primary" size= "sm" onClick ={() => plusOne(productId)}>+</Button>
							      </Card.Body>
							      <Card.Footer>
							      	<Button variant="primary" size= "sm" onClick={() => createCart(productId,quantity)}>Add to Cart</Button>{' '}
      								<Button variant="success" size= "sm" >Checkout</Button>
							      </Card.Footer>
							    </Card>
						</Col>
					</Row>
				</Container>


	)
}


