import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {Card, CardGroup, Button, Col} from "react-bootstrap";

export default function ProductCard({productProp}) {
  
  const {_id, name, description, price, stocks} = productProp;

  return (
  			
      <div className= "col-11 col-md-6 col-lg-3 mx-0 mb-4">
        <div className= "p-0 overflow-hidden  shadow">
          <div>
                  <Card>
                    <Card.Body>
                      <Card.Title>
                      <h2>{name}</h2>
                      </Card.Title>
                      <Card.Text>
                        {description}
                      </Card.Text>
                      <Card.Text>
                       Price: {price}
                      </Card.Text>
                      <Card.Text>
                       Stocks: {stocks}
                      </Card.Text>
                      <Button as={Link} to={`/productview/${_id}`} variant="primary">Details</Button>
                    </Card.Body>
                  </Card>
          </div>
        </div>
      </div>

  )
}
