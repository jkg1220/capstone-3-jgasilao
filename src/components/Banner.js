import {Container,Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	console.log(data);

	const {title, content, destination, label} = data;

	return(
	<Container>	
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<img src="./Home-Appliance-PNG-Image-HD.png" className= "img-fluid"/>
			</Col>
			<div className="mb-2 text-center">
			<Button as={Link} to="/product" variant="primary" size= "lg">{label}</Button>
			</div>
		</Row>
	</Container>
	)
}
