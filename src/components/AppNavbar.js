import { Link } from "react-router-dom";
import {Container, Navbar, Nav,} from "react-bootstrap";
import { useState ,useContext,useEffect} from 'react';
import UserContext from "../UserContext";
export default function AppNavBar(){
const {user} = useContext(UserContext);
	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand>
          	<img
              src="../jklogo.png"
              width="50"
              height="50"
              className="d-inline-block align-top"
            />
          </Navbar.Brand>
	        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
	        <Navbar.Collapse id="responsive-navbar-nav">
	          <Nav className="ms-auto" defaultActiveKey="/">
	            
	            { (user.id!==null)
	            	?	
	            		<>
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            		<Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
		            	<Nav.Link as={Link} to="/product" eventKey="/product">Product</Nav.Link>
	            		</>

	            	:
	            	<>
	            	<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            <Nav.Link as={Link} to="/registration" eventKey="/registration">Register</Nav.Link>
	            	</>

	            }

	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}